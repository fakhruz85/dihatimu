<?php
namespace Dihatimu\Love;

class Calculate{
    private $maleName, $femaleName;

    public function __construct($maleName, $femaleName){
        $this->maleName = $maleName;
        $this->femaleName = $femaleName;
    }

    public static function love($maleName, $femaleName){
        // $this->maleName = $maleName;
        // $this->femaleName = $femaleName;

        $total = strlen($maleName)+strlen($femaleName);
        $total = rand($total/2, 100);
        $data = ["male" => $maleName, "female"=> $femaleName, "result"=> $total];
        return json_encode($data);
    }
}