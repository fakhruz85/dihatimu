# dihatimu
Generate love percentange based on couple name
for learning purpose only

## Installation
This project using composer.
```
$ composer require fakhruz85/dihatimu
```

## Usage
Generate love percentange based on couple name.
```php
<?php

use Dihatimu\Love\Calculate;

Calculate::love('ahmad albab daniel', 'siti rogayah');